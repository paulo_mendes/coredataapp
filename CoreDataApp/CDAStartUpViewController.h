//
//  CDAStartUpViewController.h
//  CoreDataApp
//
//  Created by Paulo Mendes on 11/5/13.
//  Copyright (c) 2013 Paulo Mendes. All rights reserved.
//

#import "CDAStartupDataController.h"
#import <UIKit/UIKit.h>

@interface CDAStartUpViewController : CDAStartupDataController

@end
