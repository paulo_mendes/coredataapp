//
//  CDALessonsDataViewController.m
//  CoreDataApp
//
//  Created by Paulo Mendes on 11/6/13.
//  Copyright (c) 2013 Paulo Mendes. All rights reserved.
//

#import "CDALessonsDataViewController.h"
#import "CDADocumentHandler.h"
#import "Discipline.h"
#import "Lesson+S3.h"

@interface CDALessonsDataViewController ()

@end

@implementation CDALessonsDataViewController

- (void)loadLessonsFromJson {
    [self.refreshControl beginRefreshing];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSString *jsonFile = [NSString stringWithFormat:@"lessons_%@", self.discipline.identifier];
        
        NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:jsonFile ofType:@"json"]];
        NSMutableDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        [[CDADocumentHandler sharedDocumentHandler] performWithDocument:^(UIManagedDocument *document) {
            [document.managedObjectContext performBlock:^{
                for (NSDictionary *lessonDict in dict[@"data"][@"lessons"]) {
                    [Lesson insertLessonsFromDictionary:lessonDict inContext:document.managedObjectContext];
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.refreshControl endRefreshing];
                    [self setUpFetchResults];
                });
            }];
        }];
        
    });
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"lesson-cell"];
    
    Lesson *lesson = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = lesson.name;
    cell.detailTextLabel.text = lesson.discipline.name;
    
    return cell;
}

- (void)setUpFetchResults {
    if (self.discipline.managedObjectContext) {
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Lesson"];
        request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name"
                                                                  ascending:YES
                                                                   selector:@selector(localizedCaseInsensitiveCompare:)]];
        request.predicate = [NSPredicate predicateWithFormat:@"discipline = %@", self.discipline];
        
        self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                            managedObjectContext:self.discipline.managedObjectContext
                                                                              sectionNameKeyPath:nil
                                                                                       cacheName:nil];
    } else {
        self.fetchedResultsController = nil;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([self.discipline.lessons count] == 0) {
        [self loadLessonsFromJson];
    } else {
        [self setUpFetchResults];
        [self.tableView reloadData];
    }
}

@end
