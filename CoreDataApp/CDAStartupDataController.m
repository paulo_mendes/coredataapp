//
//  CDAStartupDataController.m
//  CoreDataApp
//
//  Created by Paulo Mendes on 11/5/13.
//  Copyright (c) 2013 Paulo Mendes. All rights reserved.
//

#import "CDAStartupDataController.h"
#import "Discipline.h"
#import "CDALessonsViewController.h"

@interface CDAStartupDataController ()

@end

@implementation CDAStartupDataController

- (void)setManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    _managedObjectContext = managedObjectContext;
    if (managedObjectContext) {
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Discipline"];
        //No Predicate Means All Disciplines
        request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name"
                                                                  ascending:YES
                                                                   selector:@selector(localizedCaseInsensitiveCompare:)]];
        request.predicate = nil;
        self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                            managedObjectContext:managedObjectContext
                                                                              sectionNameKeyPath:nil
                                                                                       cacheName:nil];
    } else {
        self.fetchedResultsController = nil;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *indexPath = nil;
    if ([sender isKindOfClass:[UITableViewCell class]]) {
        indexPath = [self.tableView indexPathForCell:sender];
    }
    
    if (indexPath) {
        Discipline *discipline = [self.fetchedResultsController objectAtIndexPath:indexPath];
        CDALessonsViewController *destination = [segue destinationViewController];
        destination.discipline = discipline;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"discipline-cell"];
    
    Discipline *discipline = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = discipline.name;
    cell.detailTextLabel.text = discipline.identifier;
    
    return cell;
}


@end
