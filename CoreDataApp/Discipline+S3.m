//
//  Discipline+S3.m
//  CoreDataApp
//
//  Created by Paulo Mendes on 10/29/13.
//  Copyright (c) 2013 Paulo Mendes. All rights reserved.
//

#import "Discipline+S3.h"

@implementation Discipline (S3)

+ (Discipline *)disciplineFromDatabaseWithIdentifier:(NSString *)identifier inContext:(NSManagedObjectContext *)context{
    Discipline *discipline = nil;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Discipline"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]];
    request.predicate = [NSPredicate predicateWithFormat:@"identifier = %@", identifier];
    
    NSError *error = nil;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    
    if ([matches count] == 1) {
        discipline = [matches lastObject];
    }
    
    return discipline;
}

+ (Discipline *)insertDisciplinesFromDictionary:(NSDictionary *)dict inContext:(NSManagedObjectContext *)context{
    Discipline *discipline = nil;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Discipline"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]];
    request.predicate = [NSPredicate predicateWithFormat:@"identifier = %@", dict[@"id"]];
    
    NSError *error = nil;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    
    if (![matches count]) {
        //Inserting new object on database
        discipline = [NSEntityDescription insertNewObjectForEntityForName:@"Discipline" inManagedObjectContext:context];
        
        discipline.identifier = [dict[@"id"] description];
        discipline.name = [dict[@"name"] description];

    } else if ([matches count] == 1) {
        discipline = [matches lastObject];
    } else {
        //handle some kind of error
    }
    
    return discipline;
}

@end
