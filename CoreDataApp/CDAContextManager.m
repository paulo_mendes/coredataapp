//
//  CDAContextManager.m
//  CoreDataApp
//
//  Created by Paulo Mendes on 10/29/13.
//  Copyright (c) 2013 Paulo Mendes. All rights reserved.
//

#import "CDAContextManager.h"

@implementation CDAContextManager

+ (id)sharedInstance {
    static dispatch_once_t once;
    __strong static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[CDAContextManager alloc] init];
    });
    return sharedInstance;
}

- (UIManagedDocument *)document {
    NSURL *url = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentationDirectory inDomains:NSUserDomainMask] lastObject];
    url = [url URLByAppendingPathComponent:@"AppDatabase"];
    UIManagedDocument *document = [[UIManagedDocument alloc] initWithFileURL:url];
    return document;
//    if (![[NSFileManager defaultManager] fileExistsAtPath:[url path]]) {
//        _document = document;
//    } else if (document.documentState == UIDocumentStateClosed) {
//        [document openWithCompletionHandler:^(BOOL success) {
//            if (success) {
//                _context = document.managedObjectContext;
//            }
//        }];
//    } else {
//        _context = document.managedObjectContext;
//    }
//    
//    if (_context) {
//        return _context;
//    } else {
//        _context = document.managedObjectContext;
//        return document.managedObjectContext;
//    }

}

- (NSManagedObjectContext  *)defaultContext {
    NSURL *url = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentationDirectory inDomains:NSUserDomainMask] lastObject];
    url = [url URLByAppendingPathComponent:@"AppDatabase"];
    UIManagedDocument *document = [[UIManagedDocument alloc] initWithFileURL:url];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[url path]]) {
        [document saveToURL:url forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success) {
            if (success) {
                _context = document.managedObjectContext;
            }
        }];
    } else if (document.documentState == UIDocumentStateClosed) {
        [document openWithCompletionHandler:^(BOOL success) {
            if (success) {
                _context = document.managedObjectContext;
            }
        }];
    } else {
        _context = document.managedObjectContext;
    }
    
    if (_context) {
        return _context;
    } else {
        _context = document.managedObjectContext;
        return document.managedObjectContext;
    }
}

- (id)init {
    if (self = [super init]) {
        //..
    }
    return self;
}

@end
