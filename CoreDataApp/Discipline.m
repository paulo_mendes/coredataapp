//
//  Discipline.m
//  CoreDataApp
//
//  Created by Paulo Mendes on 11/7/13.
//  Copyright (c) 2013 Paulo Mendes. All rights reserved.
//

#import "Discipline.h"
#import "Lesson.h"


@implementation Discipline

@dynamic identifier;
@dynamic name;
@dynamic lessons;

@end
