//
//  Lesson.m
//  CoreDataApp
//
//  Created by Paulo Mendes on 11/7/13.
//  Copyright (c) 2013 Paulo Mendes. All rights reserved.
//

#import "Lesson.h"
#import "Discipline.h"


@implementation Lesson

@dynamic identifier;
@dynamic name;
@dynamic pdfUrl;
@dynamic videDownloadUrl;
@dynamic videoUrl;
@dynamic discipline;

@end
