//
//  CDADocumentHandler.h
//  CoreDataApp
//
//  Created by Paulo Mendes on 11/6/13.
//  Copyright (c) 2013 Paulo Mendes. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^OnDocumentReady) (UIManagedDocument *document);

@interface CDADocumentHandler : NSObject

@property (strong, nonatomic) UIManagedDocument *document;

+ (CDADocumentHandler *)sharedDocumentHandler;
- (void)performWithDocument:(OnDocumentReady)onDocumentReady;

@end
