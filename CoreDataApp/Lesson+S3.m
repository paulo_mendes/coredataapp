//
//  Lesson+S3.m
//  CoreDataApp
//
//  Created by Paulo Mendes on 11/7/13.
//  Copyright (c) 2013 Paulo Mendes. All rights reserved.
//

#import "Lesson+S3.h"
#import "Discipline+S3.h"

@implementation Lesson (S3)

+ (Lesson *)insertLessonsFromDictionary:(NSDictionary *)dict inContext:(NSManagedObjectContext *)context{
    Lesson *lesson = nil;
    

    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Lesson"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]];
    request.predicate = [NSPredicate predicateWithFormat:@"identifier = %@", dict[@"id"]];
    
    NSError *error = nil;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    
    if (![matches count]) {
        //Inserting new object on database
        lesson = [NSEntityDescription insertNewObjectForEntityForName:@"Lesson" inManagedObjectContext:context];
        
        lesson.identifier = [dict[@"id"] description];
        lesson.name = [dict[@"title"] description];
        
        Discipline *discipline = [Discipline disciplineFromDatabaseWithIdentifier:[dict[@"categoryId"] description] inContext:context];
        lesson.discipline = discipline;
        
    } else if ([matches count] == 1) {
        lesson = [matches lastObject];
    } else {
        //handle some kind of error
    }
    
    return lesson;
}

@end
