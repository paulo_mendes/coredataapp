//
//  CDAStartupDataController.h
//  CoreDataApp
//
//  Created by Paulo Mendes on 11/5/13.
//  Copyright (c) 2013 Paulo Mendes. All rights reserved.
//

#import "CoreDataTableViewController.h"

@interface CDAStartupDataController : CoreDataTableViewController

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end
