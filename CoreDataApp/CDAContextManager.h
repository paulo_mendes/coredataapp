//
//  CDAContextManager.h
//  CoreDataApp
//
//  Created by Paulo Mendes on 10/29/13.
//  Copyright (c) 2013 Paulo Mendes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CDAContextManager : NSObject

@property (nonatomic, strong) NSManagedObjectContext *context;
@property (nonatomic, strong) UIManagedDocument *document;

+ (CDAContextManager *)sharedInstance;

- (NSManagedObjectContext  *)defaultContext;

@end
