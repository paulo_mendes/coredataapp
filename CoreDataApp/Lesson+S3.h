//
//  Lesson+S3.h
//  CoreDataApp
//
//  Created by Paulo Mendes on 11/7/13.
//  Copyright (c) 2013 Paulo Mendes. All rights reserved.
//

#import "Lesson.h"

@interface Lesson (S3)

+ (Lesson *)insertLessonsFromDictionary:(NSDictionary *)dict inContext:(NSManagedObjectContext *)context;

@end
