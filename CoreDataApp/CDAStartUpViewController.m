//
//  CDAStartUpViewController.m
//  CoreDataApp
//
//  Created by Paulo Mendes on 11/5/13.
//  Copyright (c) 2013 Paulo Mendes. All rights reserved.
//

#import "CDAStartUpViewController.h"
#import "CDADocumentHandler.h"
#import "Discipline+S3.h"

@interface CDAStartUpViewController ()

@end

@implementation CDAStartUpViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (!self.managedObjectContext) {
        [self useDocument];
    } else {
        [self refresh];
    }
}

- (void)useDocument {
    [[CDADocumentHandler sharedDocumentHandler] performWithDocument:^(UIManagedDocument *document) {
        self.managedObjectContext = document.managedObjectContext;
        [self refresh];
    }];
}

- (void)refresh {
    [self.refreshControl beginRefreshing];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"categories" ofType:@"json"]];
        NSMutableDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        [[CDADocumentHandler sharedDocumentHandler] performWithDocument:^(UIManagedDocument *document) {
            [document.managedObjectContext performBlock:^{
                for (NSDictionary *categoryDict in dict[@"data"][@"categories"]) {
                    [Discipline insertDisciplinesFromDictionary:categoryDict inContext:self.managedObjectContext];
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.refreshControl endRefreshing];
                });
            }];
        }];
        
    });
}

- (void)viewDidLoad {
    //[self.refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    
}

@end
