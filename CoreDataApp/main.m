//
//  main.m
//  CoreDataApp
//
//  Created by Paulo Mendes on 10/29/13.
//  Copyright (c) 2013 Paulo Mendes. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CDAAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CDAAppDelegate class]));
    }
}
