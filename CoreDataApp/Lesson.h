//
//  Lesson.h
//  CoreDataApp
//
//  Created by Paulo Mendes on 11/7/13.
//  Copyright (c) 2013 Paulo Mendes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Discipline;

@interface Lesson : NSManagedObject

@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * pdfUrl;
@property (nonatomic, retain) NSString * videDownloadUrl;
@property (nonatomic, retain) NSString * videoUrl;
@property (nonatomic, retain) Discipline *discipline;

@end
