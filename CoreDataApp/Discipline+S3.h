//
//  Discipline+S3.h
//  CoreDataApp
//
//  Created by Paulo Mendes on 10/29/13.
//  Copyright (c) 2013 Paulo Mendes. All rights reserved.
//

#import "Discipline.h"

@interface Discipline (S3)

+ (Discipline *)disciplineFromDatabaseWithIdentifier:(NSString *)identifier inContext:(NSManagedObjectContext *)context;
+ (Discipline *)insertDisciplinesFromDictionary:(NSDictionary *)dict inContext:(NSManagedObjectContext *)context;

@end
