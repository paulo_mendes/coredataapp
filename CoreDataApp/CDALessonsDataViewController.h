//
//  CDALessonsDataViewController.h
//  CoreDataApp
//
//  Created by Paulo Mendes on 11/6/13.
//  Copyright (c) 2013 Paulo Mendes. All rights reserved.
//

#import "CoreDataTableViewController.h"

@class Discipline;

@interface CDALessonsDataViewController : CoreDataTableViewController

@property (nonatomic, strong) Discipline *discipline;

@end
